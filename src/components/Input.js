import React from 'react';

const Input = (props) => {
  return (
    <input value={ props.Value } onChange={ props.changeValue }></input>
  )
}

export default Input;
