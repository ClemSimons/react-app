import React from 'react';

const NotFound = () => {
  return(
    <h1 className='NotFound' >Page not found</h1>
  )
}

export default NotFound;
