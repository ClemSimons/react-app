import React, { Fragment } from 'react';
import ListItem from './ListItem'


class MyData extends React.Component {

  state = {
    data: this.props.MyData
  }

  render() {

    const { data } = this.state;

      return (

        <Fragment>
          <ul>
            {Object.entries(data[0]).map((data) => (
              <ListItem key={data[0]} dataKey={data[0]} dataValue={data[1]} ></ListItem>
            ))}
          </ul>
        </Fragment>
      )
    }
}

export default MyData
