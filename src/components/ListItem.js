import React, { Fragment } from 'react';
import Input from './Input'

class ListItem extends React.Component {

  state = {
    dataValue: '',
    typeOfDataValue: typeof(this.props.dataValue)
  }

  changeValue = event => {
    const state = { ...this.state }
    const newValue = event.target.value
    state.dataValue = newValue;
    this.setState( { dataValue: newValue } )
  }

  render() {

    return(
      <Fragment>
        { ( this.state.typeOfDataValue === 'object' ) ?
          <li>{`${this.props.dataKey} =>`}
            <ul>
              {Object.entries(this.props.dataValue).map((data) => (
                <Fragment>
                  <ListItem key={data[0]} dataKey={data[0]} dataValue={data[1]} ></ListItem>
                  <Input Value={this.state.dataValue} changeValue={this.changeValue}></Input>
                </Fragment>
              ))}
            </ul>
          </li>
        :
        <li>{`${this.props.dataKey} => ${this.props.dataValue}`}</li> }
      </Fragment>
    )
  }

}

export default ListItem;
