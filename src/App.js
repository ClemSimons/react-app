import './App.css';
import React from 'react';
import MyData from './components/MyData.js';

const data = [{
  packaging:
    [{
      id:1,
      name:'Barquette PP',
      components:
        [ {
          id: 1,
          name: 'Barquette',
          materials: [{ id:1, component_id:1, name:'Polypropylène', matter:'Polypropylène', mass:150, volume:100 }]
        }, {
          id: 2,
          name: 'Opercule',
          materials: [{ id:2, component_id:2, name:'Polychlorure de vinyle', matter:'Polychlorure de vinyle', mass:20, volume:0 }]
        }, {
          id: 3,
          name: 'Cartonette',
          materials: [{ id:3, component_id:3, name:'Carton plat', matter:'Carton plat', mass:10, volume:0 }]
        } ]
    },
    {
      id:2,
      name:'Conserve acier',
      components:
        [ {
          id: 4,
          name: 'Conserve',
          materials: [{ id:4, component_id:4, name:'Acier', matter:'Acier', mass:100, volume:20 }] } ]
    },
    {
      id:3,
      name:'Caisse bois',
      components:
        [ {
          id: 5,
          name: 'Caisse',
          materials: [{ id:5, component_id:5, name:'Bois peint', matter:'Bois peint', mass:500, volume:2000 }]
        }, {
          id: 6,
          name: 'Couvercle',
          materials: [{ id:6, component_id:6, name:'Bois brut', matter:'Bois brut', mass:100, volume:0 },
                      { id:7, component_id:6, name:'Polypropylène', matter:'Polypropylène', mass:10, volume:0 }
        ] } ]
    }]
}]

class App extends React.Component {

  state = {
    data: data,
  }

  render() {

    return(
      <div className='Data-container'>
        <h1 className='App'>Packagings</h1>
        <MyData MyData={data} ></MyData>
      </div>
    )
  }
}

export default App;
